# placeYourAd-apiPlatform

## Generate DB

```
docker-compose up
docker-compose exec php bin/console d:d:c
docker-compose exec php bin/console d:s:u -f
```

## Generate JWT keys
Generate file config/jwt/private.pem and config/jwt/public.pem as shown in [documentation](https://api-platform.com/docs/core/jwt/)

---
[Production url](https://placeyourad-back-apiplat.herokuapp.com/)
---
MPD : https://gitlab.com/placeyourad/apiplatform/-/blob/master/MPD_v2.png
