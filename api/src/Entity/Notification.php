<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\NotificationRepository;
use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Core\Annotation\ApiFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"notification:read"}},
 *     denormalizationContext={"groups"={"notification:write"}},
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "validation_groups"={"Default", "create"}
 *          },
 *     },
 *     itemOperations={
 *          "get",
 *          "put"={},
 *          "delete"={}
 *     }
 * )
 * @ApiFilter(SearchFilter::class, properties={"notified": "exact"})
 * @ApiFilter(BooleanFilter::class, properties={"read"})
 * @ORM\Entity(repositoryClass=NotificationRepository::class)
 */
class Notification
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"notification:read", "notification:write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"notification:read", "notification:write"})
     */
    private $message;

    /**
     * @ORM\Column(type="boolean", length=255)
     * @Groups({"notification:read", "notification:write"})
     */
    private $read;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="notifications")
     * @Groups({"notification:read", "notification:write"})
     */
    private $notified;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"notification:read", "notification:write"})
     */
    private $link;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getRead(): ?bool
    {
        return $this->read;
    }

    public function setRead(bool $read): self
    {
        $this->read = $read;

        return $this;
    }

    public function getNotified(): ?User
    {
        return $this->notified;
    }

    public function setNotified(?User $notified): self
    {
        $this->notified = $notified;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }
}
