<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PlacementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use ApiPlatform\Core\Annotation\ApiProperty;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\NumericFilter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"placement:read"}},
 *     denormalizationContext={"groups"={"placement:write"}},
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "validation_groups"={"Default", "create"}
 *          },
 *     },
 *     itemOperations={
 *          "get",
 *          "put"={},
 *          "delete"={}
 *     }
 * )
 * @ApiFilter(PropertyFilter::class)
 * @ApiFilter(SearchFilter::class, properties={"name": "ipartial"})
 * @ApiFilter(SearchFilter::class, properties={"width": "exact"})
 * @ApiFilter(SearchFilter::class, properties={"height": "exact"})
 * @ApiFilter(SearchFilter::class, properties={"status": "exact"})
 * @ApiFilter(RangeFilter::class, properties={"price"})
 * @ApiFilter(DateFilter::class, properties={"startDate"})
 * @ApiFilter(DateFilter::class, properties={"endDate"})
 * @ORM\Entity(repositoryClass=PlacementRepository::class)
 */
class Placement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @Groups({"placement:read"})
     * @ORM\Column(type="integer")
     * @Groups({"proposal:read"})
     * @Groups({"campaign:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *     "placement:read", "placement:write",
     *     "proposal:read", "proposal:write",
     *      "campaign:read", "campaign:write"
     * })
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *     "placement:read", "placement:write",
     *      "proposal:read", "proposal:write"
     *     })
     */
    private $url;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"placement:read", "placement:write"})
     */
    private $height;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"placement:read", "placement:write"})
     */
    private $width;

    /**
     * @ORM\Column(type="float")
     * @Groups({"placement:read", "placement:write"})
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=20)
     * @Groups({"placement:read", "placement:write"})
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"placement:read", "placement:write"})
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"placement:read", "placement:write"})
     */
    private $endDate;

    /**
     * @Groups({"placement:read", "placement:write"})
     * @ORM\ManyToOne(targetEntity=Website::class, inversedBy="placements")
     */
    private $website;

    /**
     *
     * @ORM\Column(type="string", length=100000)
     * @Groups({
     *     "placement:read", "placement:write",
     *     "campaign:read", "campaign:write"
     * })
     */
    public $asset;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="placements")
     * @Groups({"placement:read", "placement:write"})
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity=Campaign::class, inversedBy="placements")
     */
    private $campaign;

    /**
     * @ORM\ManyToMany(targetEntity=Proposal::class, mappedBy="placements")
     */
    private $proposals;

    public function __construct()
    {
        $this->proposals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function setHeight(int $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function setWidth(int $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus (string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getWebsite(): ?Website
    {
        return $this->website;
    }

    public function setWebsite(?Website $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getCampaign(): ?Campaign
    {
        return $this->campaign;
    }

    public function setCampaign(?Campaign $campaign): self
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * @return Collection|Proposal[]
     */
    public function getProposals(): Collection
    {
        return $this->proposals;
    }

    public function addProposal(Proposal $proposal): self
    {
        if (!$this->proposals->contains($proposal)) {
            $this->proposals[] = $proposal;
            $proposal->addPlacement($this);
        }

        return $this;
    }

    public function removeProposal(Proposal $proposal): self
    {
        if ($this->proposals->contains($proposal)) {
            $this->proposals->removeElement($proposal);
            $proposal->removePlacement($this);
        }

        return $this;
    }
}
