<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CampaignRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ApiResource(
 *     normalizationContext={"groups"={"campaign:read"}},
 *     denormalizationContext={"groups"={"campaign:write"}},
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "validation_groups"={"Default", "create"}
 *          },
 *     },
 *     itemOperations={
 *          "get",
 *          "put"={},
 *          "delete"={}
 *     }
 * )
 * @ORM\Entity(repositoryClass=CampaignRepository::class)
 */
class Campaign
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"campaign:read", "campaign:write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"campaign:read", "campaign:write"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"campaign:read", "campaign:write"})
     */
    private $state;

    /**
     * @ORM\ManyToMany(targetEntity=Website::class, inversedBy="campaigns")
     * @Groups({"campaign:read", "campaign:write"})
     */
    private $websites;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"campaign:read", "campaign:write"})
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"campaign:read", "campaign:write"})
     */
    private $endDate;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="publisherCampaigns")
     * @Groups({"campaign:read", "campaign:write"})
     */
    private $publishers;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="advertiserCampaigns")
     * @Groups({"campaign:read", "campaign:write"})
     */
    private $advertiser;

    /**
     * @ORM\OneToMany(targetEntity=Placement::class, mappedBy="campaign", cascade={"all"})
     * @Groups({"campaign:read", "campaign:write"})
     */
    private $placements;

    /**
     * @ORM\OneToMany(targetEntity=Proposal::class, mappedBy="campaign")
     */
    private $proposals;

    public function __construct()
    {
        $this->websites = new ArrayCollection();
        $this->publishers = new ArrayCollection();
        $this->placements = new ArrayCollection();
        $this->proposals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return Collection|Website[]
     */
    public function getWebsites(): Collection
    {
        return $this->websites;
    }

    public function addWebsite(Website $website): self
    {
        if (!$this->websites->contains($website)) {
            $this->websites[] = $website;
        }

        return $this;
    }

    public function removeWebsite(Website $website): self
    {
        if ($this->websites->contains($website)) {
            $this->websites->removeElement($website);
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getPublishers(): Collection
    {
        return $this->publishers;
    }

    public function addPublisher(User $publisher): self
    {
        if (!$this->publishers->contains($publisher)) {
            $this->publishers[] = $publisher;
            $publisher->setCampaign($this);
        }

        return $this;
    }

    public function removePublisher(User $publisher): self
    {
        if ($this->publishers->contains($publisher)) {
            $this->publishers->removeElement($publisher);
            // set the owning side to null (unless already changed)
            if ($publisher->getCampaign() === $this) {
                $publisher->setCampaign(null);
            }
        }

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getAdvertiser(): ?User
    {
        return $this->advertiser;
    }

    public function setAdvertiser(?User $advertiser): self
    {
        $this->advertiser = $advertiser;

        return $this;
    }

    /**
     * @return Collection|Placement[]
     */
    public function getPlacements(): Collection
    {
        return $this->placements;
    }

    public function addPlacement(Placement $placement): self
    {
        if (!$this->placements->contains($placement)) {
            $this->placements[] = $placement;
            $placement->setCampaign($this);
        }

        return $this;
    }

    public function removePlacement(Placement $placement): self
    {
        if ($this->placements->contains($placement)) {
            $this->placements->removeElement($placement);
            // set the owning side to null (unless already changed)
            if ($placement->getCampaign() === $this) {
                $placement->setCampaign(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Proposal[]
     */
    public function getProposals(): Collection
    {
        return $this->proposals;
    }

    public function addProposal(Proposal $proposal): self
    {
        if (!$this->proposals->contains($proposal)) {
            $this->proposals[] = $proposal;
            $proposal->setCampaign($this);
        }

        return $this;
    }

    public function removeProposal(Proposal $proposal): self
    {
        if ($this->proposals->contains($proposal)) {
            $this->proposals->removeElement($proposal);
            // set the owning side to null (unless already changed)
            if ($proposal->getCampaign() === $this) {
                $proposal->setCampaign(null);
            }
        }

        return $this;
    }
}
