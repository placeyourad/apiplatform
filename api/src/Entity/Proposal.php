<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProposalRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"proposal:read"}},
 *     denormalizationContext={"groups"={"proposal:write"}},
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "validation_groups"={"Default", "create"}
 *          },
 *     },
 *     itemOperations={
 *          "get",
 *          "put"={},
 *          "delete"={}
 *     }
 * )
 * @ApiFilter(SearchFilter::class, properties={"advertiser": "exact"})
 * @ApiFilter(SearchFilter::class, properties={"publisher": "exact"})
 * @ORM\Entity(repositoryClass=ProposalRepository::class)
 */
class Proposal
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"proposal:read"})
     */
    private $id;

    /**
     * @Groups({"proposal:read", "proposal:write"})
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="advertiserProposals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $advertiser;

    /**
     * @Groups({"proposal:read", "proposal:write"})
     * @ORM\ManyToMany(targetEntity=Placement::class, inversedBy="proposals")
     */
    private $placements;

    /**
     * @Groups({"proposal:read", "proposal:write"})
     * @ORM\ManyToOne(targetEntity=Campaign::class, inversedBy="proposals")
     */
    private $campaign;

    /**
     * @ORM\ManyToMany(targetEntity=User::class)
     */
    private $publishers;

    public function __construct()
    {
        $this->placements = new ArrayCollection();
        $this->publishers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdvertiser(): ?User
    {
        return $this->advertiser;
    }

    public function setAdvertiser(?User $advertiser): self
    {
        $this->advertiser = $advertiser;

        return $this;
    }

    /**
     * @return Collection|Placement[]
     */
    public function getPlacements(): Collection
    {
        return $this->placements;
    }

    public function addPlacement(Placement $placement): self
    {
        if (!$this->placements->contains($placement)) {
            $this->placements[] = $placement;
        }

        return $this;
    }

    public function removePlacement(Placement $placement): self
    {
        if ($this->placements->contains($placement)) {
            $this->placements->removeElement($placement);
        }

        return $this;
    }

    public function getCampaign(): ?Campaign
    {
        return $this->campaign;
    }

    public function setCampaign(?Campaign $campaign): self
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getPublishers(): Collection
    {
        return $this->publishers;
    }

    public function addPublisher(User $publisher): self
    {
        if (!$this->publishers->contains($publisher)) {
            $this->publishers[] = $publisher;
        }

        return $this;
    }

    public function removePublisher(User $publisher): self
    {
        if ($this->publishers->contains($publisher)) {
            $this->publishers->removeElement($publisher);
        }

        return $this;
    }
}
