<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;


/**
 * @ApiResource(
 *     normalizationContext={"groups"={"user:read"}},
 *     denormalizationContext={"groups"={"user:write"}},
 *     collectionOperations={
 *          "get",
 *          "post"={
 *              "validation_groups"={"Default", "create"}
 *          },
 *     },
 *     itemOperations={
 *          "get",
 *          "put"={},
 *          "delete"={}
 *     }
 * )
 * @ApiFilter(PropertyFilter::class)
 * @ApiFilter(SearchFilter::class, properties={"email": "exact"})
 * @UniqueEntity(fields={"email"})
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @Groups({"user:read"})
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({
     *     "user:read", "user:write",
     *     "proposal:read", "proposal:write"
     * })
     * @Assert\NotBlank(groups={"create"})
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * @Groups({"user:read", "user:write"})
     * @Assert\NotBlank(groups={"create"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @Groups({"user:read", "user:write"})
     * @SerializedName("password")
     * @Assert\NotBlank(groups={"create"})
     */
    private $plainPassword;

    /**
     * @ORM\OneToMany(targetEntity=Website::class, mappedBy="owner", cascade={"persist"})
     */
    private $websites;

    /**
     * @ORM\ManyToMany(targetEntity=Campaign::class, mappedBy="publisher")
     */
    private $publisherCampaigns;

    /**
     * @ORM\OneToMany(targetEntity=Campaign::class, mappedBy="advertiser")
     */
    private $advertiserCampaigns;

    /**
     * @ORM\OneToMany(targetEntity=Proposal::class, mappedBy="advertiser", orphanRemoval=true)
     */
    private $advertiserProposals;

    /**
     * @ORM\OneToMany(targetEntity=Notification::class, mappedBy="notified")
     */
    private $notifications;

    /**
     * @ORM\OneToMany(targetEntity=Placement::class, mappedBy="owner")
     */
    private $placements;

    public function __construct()
    {
        $this->websites = new ArrayCollection();
        $this->publisherCampaigns = new ArrayCollection();
        $this->advertiserCampaigns = new ArrayCollection();
        $this->advertiserProposals = new ArrayCollection();
        $this->notifications = new ArrayCollection();
        $this->placements = new ArrayCollection();
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
         $this->plainPassword = null;
    }

    /**
     * @return Collection|Website[]
     */
    public function getWebsites(): Collection
    {
        return $this->websites;
    }

    public function addWebsite(Website $website): self
    {
        if (!$this->websites->contains($website)) {
            $this->websites[] = $website;
            $website->setOwner($this);
        }

        return $this;
    }

    public function removeWebsite(Website $website): self
    {
        if ($this->websites->contains($website)) {
            $this->websites->removeElement($website);
            // set the owning side to null (unless already changed)
            if ($website->getOwner() === $this) {
                $website->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Campaign[]
     */
    public function getPublisherCampaigns(): Collection
    {
        return $this->publisherCampaigns;
    }

    public function addPublisherCampaign(Campaign $publisherCampaign): self
    {
        if (!$this->publisherCampaigns->contains($publisherCampaign)) {
            $this->publisherCampaigns[] = $publisherCampaign;
            $publisherCampaign->setPublisher($this);
        }

        return $this;
    }

    public function removePublisherCampaign(Campaign $publisherCampaign): self
    {
        if ($this->publisherCampaigns->contains($publisherCampaign)) {
            $this->publisherCampaigns->removeElement($publisherCampaign);
            // set the owning side to null (unless already changed)
            if ($publisherCampaign->getPublisher() === $this) {
                $publisherCampaign->setPublisher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Campaign[]
     */
    public function getAdvertiserCampaigns(): Collection
    {
        return $this->advertiserCampaigns;
    }

    public function addAdvertiserCampaign(Campaign $advertiserCampaign): self
    {
        if (!$this->advertiserCampaigns->contains($advertiserCampaign)) {
            $this->advertiserCampaigns[] = $advertiserCampaign;
            $advertiserCampaign->setAdvertiser($this);
        }

        return $this;
    }

    public function removeAdvertiserCampaign(Campaign $advertiserCampaign): self
    {
        if ($this->advertiserCampaigns->contains($advertiserCampaign)) {
            $this->advertiserCampaigns->removeElement($advertiserCampaign);
            // set the owning side to null (unless already changed)
            if ($advertiserCampaign->getAdvertiser() === $this) {
                $advertiserCampaign->setAdvertiser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Proposal[]
     */
    public function getAdvertiserProposals(): Collection
    {
        return $this->advertiserProposals;
    }

    public function addAdvertiserProposal(Proposal $advertiserProposal): self
    {
        if (!$this->advertiserProposals->contains($advertiserProposal)) {
            $this->advertiserProposals[] = $advertiserProposal;
            $advertiserProposal->setAdvertiser($this);
        }

        return $this;
    }

    public function removeAdvertiserProposal(Proposal $advertiserProposal): self
    {
        if ($this->advertiserProposals->contains($advertiserProposal)) {
            $this->advertiserProposals->removeElement($advertiserProposal);
            // set the owning side to null (unless already changed)
            if ($advertiserProposal->getAdvertiser() === $this) {
                $advertiserProposal->setAdvertiser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setNotified($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->contains($notification)) {
            $this->notifications->removeElement($notification);
            // set the owning side to null (unless already changed)
            if ($notification->getNotified() === $this) {
                $notification->setNotified(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Placement[]
     */
    public function getPlacements(): Collection
    {
        return $this->placements;
    }

    public function addPlacement(Placement $placement): self
    {
        if (!$this->placements->contains($placement)) {
            $this->placements[] = $placement;
            $placement->setOwner($this);
        }

        return $this;
    }

    public function removePlacement(Placement $placement): self
    {
        if ($this->placements->contains($placement)) {
            $this->placements->removeElement($placement);
            // set the owning side to null (unless already changed)
            if ($placement->getOwner() === $this) {
                $placement->setOwner(null);
            }
        }

        return $this;
    }
}
