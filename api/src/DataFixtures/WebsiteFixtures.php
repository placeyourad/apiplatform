<?php

namespace App\DataFixtures;

use App\Entity\Placement;
use App\Entity\User;
use App\Entity\Website;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WebsiteFixtures extends Fixture implements DependentFixtureInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        $allusers = $manager->getRepository(User::class)->findAll();
        $publishers = [];
        $advertisers = [];

        foreach ($allusers as $user)
        {
            if(in_array('ROLE_PUBLISHER', $user->getRoles())){
                array_push($publishers, $user);

        }
            elseif (in_array('ROLE_ADVERTISER', $user->getRoles())){
                $advertisers[] = $user;
            }
        }

        for ($i = 0; $i < 50; $i++) {
            $website = (new Website())
            ->setUrl($faker->url)
            ->setOwner($faker->randomElement($publishers));

            $placement = (new Placement())
            ->setWebsite($website);

            $manager->persist($placement);
        }
        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            PlacementFixtures::class
        );
    }
}
