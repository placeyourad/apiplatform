<?php

namespace App\DataFixtures;

use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;
    private $container;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 50; $i++) {
            $user = (new User())
                ->setEmail($faker->email)
                ->setPassword('string');

            if ($i % 2 == 0) {
                $user->setRoles(["ROLE_USER", "ROLE_PUBLISHER"]);
            } else {
                $user->setRoles(["ROLE_USER", "ROLE_ADVERTISER"]);
            }
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'string'
            ));
            $manager->persist($user);
        }

        $manager->flush();

    }
}
