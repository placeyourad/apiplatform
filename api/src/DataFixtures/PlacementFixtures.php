<?php

namespace App\DataFixtures;

use App\Entity\Campaign;
use App\Entity\Placement;
use App\Entity\Website;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PlacementFixtures extends Fixture
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 50; $i++) {

            $website = new Website();
            $campaign = new Campaign();

            $placement = (new Placement())
                ->setName($faker->name)
                ->setWidth(15)
                ->setStartDate($faker->dateTime('now'))
                ->setEndDate($faker->dateTime('3 months'))
                ->setWebsite($website)
                ->setCampaign($campaign)
                ->setHeight(12);

            $manager->persist($placement);
        }

        $manager->flush();

    }

}
