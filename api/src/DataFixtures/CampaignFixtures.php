<?php

namespace App\DataFixtures;

use App\Entity\Campaign;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CampaignFixtures extends Fixture
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();


        $allusers = $manager->getRepository(User::class)->findAll();
        $publishers = [];
        $advertisers = [];

        foreach ($allusers as $user)
        {
            if(in_array('ROLE_PUBLISHER', $user->getRoles())){
                array_push($publishers, $user);

            }
            elseif (in_array('ROLE_ADVERTISER', $user->getRoles())){
                $advertisers[] = $user;
            }
        }
        for ($i = 0; $i < 50; $i++) {

            $campaign = (new Campaign())
                ->setName($faker->name)
                ->setStartDate($faker->dateTime('now'))
                ->setEndDate($faker->dateTime('3 months'))
                ->setState($faker->boolean)
                ->setPublisher($publishers)
                ->setAdvertiser($advertisers);


            $manager->persist($user);
        }

        $manager->flush();

    }

    /**
     * @inheritDoc
     */
    public function getDependencies()
    {
        return array(
            UserFixtures::class
        );
    }
}
